import java.util.Scanner;
public class BoardGameApp{
	public static void main(String args[]) {
		System.out.println("Welcome");
		int numCastles = 7;
		int turns = 0;
		
		int row = 0;
		int column = 0;
		int placeTokenValue = 0;
			
		Board board = new Board();
		Scanner reader = new Scanner(System.in);
		
		//gameloop runs while the turns count doesnt reach 8 or player still has castles to place
		while(numCastles > 0 && turns < 8){
			System.out.println(board);
			System.out.println("Castles " + numCastles);
			System.out.println("Turns left: " + (8-turns));
				
			System.out.println("Input a row");
			row  = Integer.parseInt(reader.nextLine());
			System.out.println("Input a column");
			column = Integer.parseInt(reader.nextLine());
			
			//user will be asked to input new values as long as the value of the target they chose is not blank or hidden wall 
			placeTokenValue = board.placeToken(row, column);
			while(placeTokenValue < 0){
					System.out.println("Please enter new values");
					System.out.println("Input a row");
					row  = Integer.parseInt(reader.nextLine());
					System.out.println("Input a column");
					column = Integer.parseInt(reader.nextLine());
					
					placeTokenValue = board.placeToken(row, column);
			}
			if(placeTokenValue == 1){
				System.out.println("THERE WAS A WALL - you lose a turn");
				turns++;
			}else if(placeTokenValue == 0){
				System.out.println("Castle tile successfully placed");
				turns++;
				numCastles--;
				
			}
		}
		//printing final message
		System.out.println(board);
		if(numCastles == 0){
			System.out.println("You Won!");
		}else{
			System.out.println("You lost...");
		}
	}
}
