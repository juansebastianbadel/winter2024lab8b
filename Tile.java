public enum Tile {
	//enum values
	BLANK("_"),
	HIDDEN_WALL("_"),
	WALL("W"),
	CASTLE("C");

	private String name;

	private Tile(String tile) {
		this.name = tile;
	}
	
	//allows for the desired string to be taken from each enum value (ex _ instead of BLANK)
	public String getName() {
		return this.name;
	}
}
