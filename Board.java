import java.util.Random;
public class Board {
	private Tile[][] grid;
	private final int gameLength = 5;
	private Random rng;

	public Board() {
		this.grid = new Tile[this.gameLength][this.gameLength];
		rng = new Random();
		//every value is initialized to blank in constructor except the random hidden_walls
		for(int j = 0; j < grid.length; j++) {

			int randIndex = rng.nextInt(grid[j].length);
			for(int i = 0; i < grid[j].length; i++) {
				if(i == randIndex){
					grid[j][i] = Tile.HIDDEN_WALL;
				}
				else{
					grid[j][i] = Tile.BLANK;
				}
			}
		}
	}

	public String toString() {
		//printing board
		String all = "";
		for(int j = 0; j < grid.length; j++) {
			for(int i = 0; i < grid[j].length; i++) {
				all += grid[j][i].getName() + " ";
			}
			all += "\n";
		}
		return all;
	}
	
	public int placeToken(int row, int col) {
		//-2 is returned if col or row values are not within board length
		if(row < 0 || row > gameLength-1 || col < 0 || col > gameLength-1){
			return -2;
		}
		//-1 is returned if the position has a castle or wall
		else if(grid[row][col].equals(Tile.CASTLE) || grid[row][col].equals(Tile.WALL)){
			return -1;
		}
		// hidden wall returns 1
		else if(grid[row][col].equals(Tile.HIDDEN_WALL)){
			grid[row][col] = Tile.WALL;
			return 1;
		}
		//anything else, aka a blank value, will return 0
		grid[row][col] = Tile.CASTLE;
		return 0;
	}
	
}